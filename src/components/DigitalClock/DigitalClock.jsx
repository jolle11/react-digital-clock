import './DigitalClock.scss';
import React, { useEffect, useState } from 'react';

const DigitalClock = () => {
    const [clockState, setClockState] = useState();
    useEffect(() => {
        setInterval(() => {
            const date = new Date();
            setClockState(date.toLocaleTimeString());
        }, 10);
    }, []);

    return (
        <div className="digitalclock">
            <h1 className="digitalclock__title">Digital Clock</h1>
            <h2 className="digitalclock__clock">{clockState}</h2>
        </div>
    );
};

export default DigitalClock;
