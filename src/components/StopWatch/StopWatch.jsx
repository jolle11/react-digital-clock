import './StopWatch.scss';
import React, { useState, useEffect } from 'react';

const StopWatch = () => {
    const [time, setTime] = useState(0);
    const [timerOn, setTimerOn] = useState(false);

    useEffect(() => {
        let interval = null;
        if (timerOn) {
            interval = setInterval(() => {
                setTime((prevTime) => prevTime + 10);
            }, 10);
        } else {
            clearInterval(interval);
        }
        return () => clearInterval(interval);
    }, [timerOn]);

    return (
        <div className="stopwatch">
            <h1 className="stopwatch__title">Stopwatch</h1>
            <div className="stopwatch__watch">
                <span>{('0' + Math.floor((time / 60000) % 60)).slice(-2)}:</span>
                <span>{('0' + Math.floor((time / 1000) % 60)).slice(-2)}:</span>
                <span>{('0' + ((time / 10) % 100)).slice(-2)}</span>
            </div>
            <div id="buttons" className="stopwatch__buttons">
                {!timerOn && time === 0 && (
                    <button className="stopwatch__buttons stopwatch__buttons--start" onClick={() => setTimerOn(true)}>
                        Start
                    </button>
                )}
                {timerOn && (
                    <button className="stopwatch__buttons stopwatch__buttons--stop" onClick={() => setTimerOn(false)}>
                        Stop
                    </button>
                )}
                {!timerOn && time > 0 && (
                    <button className="stopwatch__buttons stopwatch__buttons--resume" onClick={() => setTimerOn(true)}>
                        Resume
                    </button>
                )}
                {!timerOn && time > 0 && (
                    <button className="stopwatch__buttons stopwatch__buttons--reset" onClick={() => setTime(0)}>
                        Reset
                    </button>
                )}
            </div>
        </div>
    );
};

export default StopWatch;
