import './CountDown.scss';
import React, { useEffect, useState } from 'react';

const CountDown = () => {
    const [time, setTime] = useState('');
    useEffect(() => {
        let countDownDate = new Date('Jan 30, 2022 23:59:00').getTime();
        let x = setInterval(() => {
            let now = new Date().getTime();
            let distance = countDownDate - now;
            let days = Math.floor(distance / (1000 * 60 * 60 * 24));
            let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            let seconds = Math.floor((distance % (1000 * 60)) / 1000);
            setTime(days + 'd ' + hours + 'h ' + minutes + 'm ' + seconds + 's');
            if (distance < 0) {
                clearInterval(x);
                setTime('Countdown finished!');
            }
        }, 10);
    }, []);
    return (
        <div className="countdown">
            <h1 className="countdown__title">Countdown</h1>
            <h2 className="countdown__time">{time}</h2>
        </div>
    );
};

export default CountDown;
