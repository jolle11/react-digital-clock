import './App.scss';
import { Routes, Route, Link } from 'react-router-dom';
import { DigitalClock, CountDown, StopWatch } from './components';

function App() {
    return (
        <div className="app">
            <nav className="app__nav">
                <ul className="app__list">
                    <Link to="/" className="app__item">
                        Digital Clock
                    </Link>
                    <Link to="/countdown" className="app__item">
                        Countdown
                    </Link>
                    <Link to="/stopwatch" className="app__item">
                        Stopwatch
                    </Link>
                </ul>
            </nav>
            <Routes>
                <Route path="/" element={<DigitalClock />}></Route>
                <Route path="/countdown" element={<CountDown />}></Route>
                <Route path="/stopwatch" element={<StopWatch />}></Route>
            </Routes>
        </div>
    );
}

export default App;
